

CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE TABLE "public"."lobbies"("id" uuid NOT NULL DEFAULT gen_random_uuid(), "game_id" uuid, "user_id" uuid NOT NULL, "created_at" timestamptz NOT NULL DEFAULT now(), "name" text NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("user_id") REFERENCES "public"."users"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("game_id") REFERENCES "public"."games"("id") ON UPDATE restrict ON DELETE restrict);