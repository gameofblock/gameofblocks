
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE TABLE "public"."users"("id" uuid NOT NULL DEFAULT gen_random_uuid(), "auth_id" text NOT NULL, "created_at" timestamptz NOT NULL DEFAULT now(), "username" text, "last_login" timestamptz NOT NULL DEFAULT now(), "picture" text, "active" boolean NOT NULL DEFAULT true, "email" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("auth_id"));