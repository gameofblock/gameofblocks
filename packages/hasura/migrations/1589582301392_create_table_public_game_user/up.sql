CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE TABLE "public"."game_user"("id" uuid NOT NULL DEFAULT gen_random_uuid(), "user_id" text NOT NULL, "game_id" uuid NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("user_id") REFERENCES "public"."users"("auth_id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("game_id") REFERENCES "public"."games"("id") ON UPDATE restrict ON DELETE restrict);
