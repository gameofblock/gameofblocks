

ALTER TABLE "public"."games" ADD COLUMN "current_turn" integer NULL DEFAULT 0;

ALTER TABLE "public"."games" ADD COLUMN "current_player" integer NULL DEFAULT 0;

ALTER TABLE "public"."games" ADD COLUMN "current_player_id" text NULL;

ALTER TABLE "public"."games" ADD COLUMN "players" json NULL;
