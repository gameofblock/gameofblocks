
ALTER TABLE "public"."games" DROP COLUMN "players";

ALTER TABLE "public"."games" DROP COLUMN "current_player_id";

ALTER TABLE "public"."games" DROP COLUMN "current_player";

ALTER TABLE "public"."games" ADD COLUMN "current_player" text;
ALTER TABLE "public"."games" ALTER COLUMN "current_player" DROP NOT NULL;
ALTER TABLE "public"."games" ALTER COLUMN "current_player" SET DEFAULT '0'::text;

ALTER TABLE "public"."games" DROP COLUMN "current_turn";

ALTER TABLE "public"."games" DROP COLUMN "current_player";
