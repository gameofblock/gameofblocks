import * as path from 'path';
import express, { Application } from 'express';
import errorHandler from '../api/middlewares/error.handler';
import { OpenApiValidator } from 'express-openapi-validator';
import env from '@gameofblocks/env';

export default function (
  app: Application,
  routes: (app: Application) => void
): Promise<void> {
  const apiSpec = path.join(__dirname, 'api.yml');
  const validateResponses = !!(
    env.OPENAPI_ENABLE_RESPONSE_VALIDATION &&
    env.OPENAPI_ENABLE_RESPONSE_VALIDATION.toLowerCase() === 'true'
  );
  return new OpenApiValidator({
    apiSpec,
    validateResponses,
  })
    .install(app)
    .then(() => {
      app.use(env.OPENAPI_SPEC || '/spec', express.static(apiSpec));
      routes(app);
      app.use(errorHandler);
    });
}
