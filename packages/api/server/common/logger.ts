import pino from 'pino';
import env from '@gameofblocks/env';

const l = pino({
  name: env.APP_ID,
  level: env.LOG_LEVEL,
});

export default l;
