import ApolloClient from 'apollo-boost';
import 'cross-fetch/polyfill';
import env from '@gameofblocks/env';

export const client = new ApolloClient({
  uri: env.GRAPHQL_SERVER_URI,
  headers: { 'x-hasura-admin-secret': env.HASURA_SECRET },
});
