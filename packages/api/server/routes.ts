import { Application } from 'express';
import gamesRouter from './api/controllers/games/router';
import gameEventsRouter from './api/controllers/game_events/router';

export default function routes(app: Application): void {
  app.use('/api/games', gamesRouter);
  app.use('/api/games-events', gameEventsRouter);
}
