import { gamesService } from '../../services';
import { Request, Response } from 'express';

export class Controller {
  create(req: Request, res: Response): void {
    if (
      req.body.event.op === 'INSERT' &&
      req.body.trigger.name === 'game_events'
    ) {
      console.log(req.body.event.data.new);
      gamesService.send('LOCAL.INCREMENT', {
        id: req.body.event.data.new.game_id,
      });
    }
    res.json({ success: true });
  }
}

export default new Controller();
