import { gamesService } from '../../services';
import { Request, Response } from 'express';

export class Controller {
  create(req: Request, res: Response): void {
    if (req.body.event.op === 'INSERT' && req.body.trigger.name === 'game') {
      gamesService.send('NEW_GAME.CREATE', req.body.event.data.new);
    }
    res.json({ success: true });
  }
}

export default new Controller();
