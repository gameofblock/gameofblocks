import gql from 'graphql-tag';

export const UPDATE_GAME = gql`
  mutation UpdateGame(
    $id: uuid
    $currentTurn: Int
    $currentPlayer: Int
    $currentPlayerId: String
  ) {
    update_games(
      where: { id: { _eq: $id } }
      _set: {
        current_player: $currentPlayer
        current_turn: $currentTurn
        current_player_id: $currentPlayerId
      }
    ) {
      affected_rows
    }
  }
`;
