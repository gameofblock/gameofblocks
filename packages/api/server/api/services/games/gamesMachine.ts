import { Machine, spawn, assign, send } from 'xstate';
import { UserEvent } from '../../../types/userEvent';
import { gameMachine } from './gameMachine';

interface GamesMachineContext {
  game: string;
  games: string[];
}

const createNewGameMachine = assign({
  games: (ctx: GamesMachineContext, e: UserEvent) => {
    const { id, players } = e;
    const newGame = {
      id,
      currentPlayer: 0,
      currentPlayerId: players[0],
      currentTurn: 0,
      players,
    };
    return {
      ...ctx.games,
      [id]: {
        ...newGame,
        ref: spawn(gameMachine.withContext(newGame)),
      },
    };
  },
});

const gamesMachine = Machine<GamesMachineContext, UserEvent>(
  {
    id: 'gamesMachine',
    initial: 'create',
    context: {
      game: '',
      games: [],
    },
    states: {
      create: {
        on: {
          'NEW_GAME.CREATE': {
            target: 'create',
            actions: [
              createNewGameMachine,
              send('START', {
                to: (context: GamesMachineContext, event: UserEvent) => {
                  return context.games[event.id].ref;
                },
              }),
              'notify',
            ],
          },
          'LOCAL.INCREMENT': {
            actions: send('INCREMENT', {
              to: (context: GamesMachineContext, event: UserEvent) => {
                return context.games[event.id].ref;
              },
            }),
          },
        },
      },
      connected: {},
    },
  },
  {
    actions: {
      createNewGameMachine,
      notify: (context: GamesMachineContext, event: UserEvent): void => {
        console.log(context);
        console.log(event);
      },
    },
  }
);

export { gamesMachine };
