import { Machine, assign, actions } from 'xstate';
import { updateGame } from './game';
import { UserEvent } from '../../../types/userEvent';

const { send, cancel } = actions;

interface GameMachineContext {
  id: string;
  currentPlayer: number;
  currentPlayerId: string;
  currentTurn: number;
  players: string[];
}

const incrementPlayer = assign({
  currentPlayer: (context: GameMachineContext) => {
    const currentPlayer =
      context.currentPlayer === 3 ? 0 : context.currentPlayer + 1;
    return currentPlayer;
  },
});

const setPlayerPlayerId = assign({
  currentPlayerId: (context: GameMachineContext) => {
    return context.players[context.currentPlayer];
  },
});

const incrementTurn = assign({
  currentTurn: (context: GameMachineContext) => {
    const currentTurn =
      context.currentPlayer === 0
        ? context.currentTurn + 1
        : context.currentTurn;
    return currentTurn;
  },
});

const notify = (context: GameMachineContext, event: UserEvent): void => {
  console.log(context);
  console.log(event);
};

const syncGame = (context: GameMachineContext): void => {
  updateGame({
    id: context.id,
    currentPlayerId: context.currentPlayerId,
    currentPlayer: context.currentPlayer,
    currentTurn: context.currentTurn,
  });
};

const sendIncrementAfter10Second = send('TIMER', {
  delay: 60000,
  id: 'incrementTimer', // give the event a unique ID
});

const cancelTimer = cancel('incrementTimer');

const gameIsOver = (context): boolean => {
  return context.currentTurn === 10 && context.currentPlayer === 3;
};

export const gameMachine = Machine<GameMachineContext>(
  {
    id: 'game',
    initial: 'idle',
    context: {
      id: undefined,
      currentPlayer: 0,
      currentPlayerId: '',
      currentTurn: 0,
      players: [],
    },
    states: {
      idle: {
        on: {
          START: {
            target: 'autoIncrementing',
            // put a sync in here for no delay
            actions: ['notify'],
          },
        },
      },
      autoIncrementing: {
        entry: sendIncrementAfter10Second,
        on: {
          '': {
            target: 'end',
            cond: 'gameIsOver',
          },
          TIMER: {
            target: 'autoIncrementing',
            actions: [
              'incrementPlayer',
              'setPlayerPlayerId',
              'incrementTurn',
              'syncGame',
              'notify',
            ],
          },
          INCREMENT: {
            target: 'autoIncrementing',
            actions: [
              cancelTimer,
              'incrementPlayer',
              'setPlayerPlayerId',
              'incrementTurn',
              'syncGame',
              'notify',
            ],
          },
        },
      },
      end: {},
    },
  },
  {
    actions: {
      incrementPlayer,
      incrementTurn,
      setPlayerPlayerId,
      syncGame,
      notify,
    },
    guards: { gameIsOver },
  }
);
