import { gamesMachine } from './gamesMachine';
import { interpret } from 'xstate';

export const gamesService = interpret(gamesMachine)
  .onTransition((state) => console.log(state.value))
  .start();
