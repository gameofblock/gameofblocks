import { client } from '../../../common/initApolloClient';
import { UPDATE_GAME } from './mutations';
import logger from '../../../common/logger';

async function updateGame({
  id,
  currentTurn,
  currentPlayer,
  currentPlayerId,
}): Promise<void> {
  const { data, errors } = await client.mutate({
    mutation: UPDATE_GAME,
    variables: { id, currentTurn, currentPlayer, currentPlayerId },
  });

  if (errors) {
    logger.error(errors);
    return null;
  }

  return data;
}

export { updateGame };
