export interface UserEvent {
  type: string;
  end_date: string;
  current_player_id: string;
  entry_prize: number;
  x_size: number;
  start_date: string;
  current_turn: number;
  prize: number;
  created_at: string;
  current_player: number;
  id: string;
  players: string[];
  max_players: string;
  y_size: string;
  turn_number: string;
}
