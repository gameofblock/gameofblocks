import './common/env';
import Server from './common/server';
import routes from './routes';
import env from '@gameofblocks/env';

const port = parseInt(env.PORT);
export default new Server().router(routes).listen(port);
