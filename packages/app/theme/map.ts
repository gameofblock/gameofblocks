// @TODO: Import Theme types from theme-ui when components are typed
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const theme: any = {
  space: [0, 4, 8, 16, 32, 48, 64, 128, 256, 512],
  fonts: {
    body: '"IndusHtest", "Avenir Next", sans-serif',
    heading: '"IndusHtest", "Avenir Next", sans-serif',
    monospace: 'Menlo, monospace',
  },
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64],
  fontWeights: {
    body: 400,
    bolder: 600,
    heading: 700,
    bold: 500,
  },
  lineHeights: {
    body: 1.5,
    heading: 1.2,
  },
  letterSpacings: {
    body: 'normal',
    caps: '0.2em',
  },
  colors: {
    text: '#5c5edc',
    background: '#06122e',
    primary: '#5c5edc',
    secondary: '#1dcc87',
    darkblue: '#08132C',
    dark: '#06122e',
    white: '#ffffff',
    plain: '#2EC4B6',
    water: '#56C3E4',
    mountain: '#3A506B',
    desert: '#FF9D00',
    forest: '#206F8B',
  },
  text: {
    paragraph: {
      fontSize: 3,
    },
  },
  images: {
    avatar: {
      bg: 'background',
      width: 48,
      height: 48,
      borderRadius: 99999,
    },
  },
  buttons: {
    primary: {
      color: 'white',
      background: '#5c5edc',
      borderRadius: '40px',
    },
    secondary: {
      color: 'white',
      background: '#1dcc87',
      borderRadius: '40px',
      width: 'fit-content',
    },
  },
  styles: {
    root: {
      fontFamily: 'body',
      lineHeight: 'body',
      fontWeight: 'body',
      h1: {
        color: 'text',
        fontFamily: 'heading',
        lineHeight: 'heading',
        fontWeight: 'heading',
        fontSize: 6,
      },
    },
  },
};

export default theme;
