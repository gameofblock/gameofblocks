import React, { FC, ReactElement } from 'react';
import Link from 'next/link';
import { useColorMode } from 'theme-ui';
import { MenuWrapper, MenuItem, MenuText } from './Style';
import Icon from '../../componentsUi/Icons';

const Menu: FC = (): ReactElement => {
  const [colorMode, setColorMode] = useColorMode();
  return (
    <MenuWrapper>
      <MenuItem
        onClick={(): any => {
          setColorMode(colorMode === 'default' ? 'dark' : 'default');
        }}
      >
        <Icon size="20px" color="white" hasLineWrapper name="notification" />
        <MenuText>Color mode</MenuText>
      </MenuItem>
      <MenuItem>
        <Icon size="20px" color="white" hasLineWrapper name="notification" />
        <MenuText>Notifications</MenuText>
      </MenuItem>
      <Link href="/api/logout">
        <MenuItem as="a">
          <Icon size="20px" color="white" hasLineWrapper name="logout" />
          <MenuText>Logout</MenuText>
        </MenuItem>
      </Link>
    </MenuWrapper>
  );
};

export { Menu };
