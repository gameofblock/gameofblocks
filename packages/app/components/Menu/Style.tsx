import styled from '@emotion/styled';
import { css, Text, Button, Flex } from 'theme-ui';
import { sx } from '../../componentsUi/Sx';

export const MenuWrapper = styled(Flex)([
  css({
    justifyContent: 'flex-end',
    padding: '2',
    zIndex: 1,
  }),
  sx,
]);

export const MenuItem = styled(Button)([
  css({
    border: 0,
    background: 'none',
    m: 0,
    outline: 'none',
    cursor: 'pointer',
    '&:hover': {
      opacity: '0.7',
    },
  }),
  sx,
]);

export const MenuText = styled(Text)([
  css({
    display: 'inline-block',
    ml: '2',
  }),
  sx,
]);
