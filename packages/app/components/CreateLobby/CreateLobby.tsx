import React, { FunctionComponent } from 'react';
import { useRouter } from 'next/router';
import { useMutation } from 'react-apollo';
import { Heading } from 'theme-ui';

import { Container } from '../../componentsUi/container';
import { CreateLobbyForm } from './CreateLobbyForm';
import {
  CREATE_LOBBY,
  CreateLobbyMutationResult,
  CreateLobbyMutationVariables,
} from './mutations';

interface CreateLobbyProps {
  userId: string;
}

export const CreateLobby: FunctionComponent<CreateLobbyProps> = (
  props: CreateLobbyProps
) => {
  const { userId } = props;
  const router = useRouter();
  const [createLobby, { loading, error }] = useMutation<
    CreateLobbyMutationResult,
    CreateLobbyMutationVariables
  >(CREATE_LOBBY, {
    onCompleted: async ({ insert_lobbies_one: lobby }) => {
      await router.replace('/lobbies/[id]', `/lobbies/${lobby.id}`);
    },
  });

  return (
    <Container>
      <Heading sx={{ mb: '4' }}>Create a new lobby</Heading>
      <CreateLobbyForm
        hasError={!!error}
        loading={loading}
        onSubmit={async ({ name }): Promise<void> => {
          await createLobby({
            variables: {
              name,
              ownerId: userId,
            },
          });
        }}
      />
    </Container>
  );
};

export default CreateLobby;
