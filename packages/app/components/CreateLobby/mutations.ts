import gql from 'graphql-tag';

import { Lobby } from '../../types/lobby';

export interface CreateLobbyMutationVariables {
  name: string;
  ownerId: string;
}

export interface CreateLobbyMutationResult {
  insert_lobbies_one: Lobby;
}

export const CREATE_LOBBY = gql`
  mutation create_lobby($name: String!, $ownerId: uuid!) {
    insert_lobbies_one(object: { name: $name, user_id: $ownerId }) {
      id
      name
      user_id
      created_at
    }
  }
`;
