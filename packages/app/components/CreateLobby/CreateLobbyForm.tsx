/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { FunctionComponent } from 'react';
import { Button, Text, Input, Label } from 'theme-ui';
import { useFormik } from 'formik';

import createLobbyValidationSchema from './createLobbyValidationSchema';

interface CreateLobbyFormProps {
  loading: boolean;
  hasError: boolean;
  onSubmit: ({ name: string }) => Promise<void>;
}

export const CreateLobbyForm: FunctionComponent<CreateLobbyFormProps> = (
  props: CreateLobbyFormProps
) => {
  const { hasError, onSubmit, loading } = props;
  const { errors, handleChange, values, handleSubmit } = useFormik({
    initialValues: {
      name: '',
    },
    validationSchema: createLobbyValidationSchema,
    onSubmit: async ({ name }, { setSubmitting }) => {
      await onSubmit({ name });
      setSubmitting(false);
    },
  });

  return (
    <form onSubmit={handleSubmit}>
      <Label mb={2} htmlFor="name">
        Name
      </Label>
      <Input
        required
        id="name"
        name="name"
        type="text"
        onChange={handleChange}
        value={values.name}
      />
      {errors.name && <Text>{errors.name}</Text>}

      {!loading && hasError && (
        <Text color="red">Oops something went wrong</Text>
      )}

      <Button disabled={loading} mt={2} type="submit">
        Submit
      </Button>
    </form>
  );
};

export default CreateLobbyForm;
