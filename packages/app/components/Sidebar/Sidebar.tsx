import React, { FC } from 'react';
import Link from 'next/link';
import Icon from '../../componentsUi/Icons';
import {
  SidebarLogo,
  SidebarLogoWrapper,
  SidebarWrapper,
  SideBarMenuItem,
  SidebarMenu,
  SidebarMenuWrapper,
} from './Style';
import { User } from '../../types/user';

interface SidebarProps {
  user?: User;
}

const Sidebar: FC<SidebarProps> = () => {
  return (
    <SidebarWrapper>
      <SidebarLogoWrapper>
        <Link href="/">
          <SidebarLogo>Game of blocks</SidebarLogo>
        </Link>
      </SidebarLogoWrapper>
      <SidebarMenuWrapper>
        <SidebarMenu>
          <Link href="/dashboard">
            <SideBarMenuItem>
              <Icon size="54px" name="dashboard" />
            </SideBarMenuItem>
          </Link>
          <Link href="/lobbies/create">
            <SideBarMenuItem>
              <Icon size="54px" name="sword" />
            </SideBarMenuItem>
          </Link>
          <Link href="/map">
            <SideBarMenuItem>
              <Icon size="54px" name="deck" />
            </SideBarMenuItem>
          </Link>
          <Link href="/profile">
            <SideBarMenuItem>
              <Icon size="54px" name="profile" />
            </SideBarMenuItem>
          </Link>
        </SidebarMenu>
      </SidebarMenuWrapper>
    </SidebarWrapper>
  );
};

export { Sidebar };
