import styled from '@emotion/styled';
import { css, Box, Flex } from 'theme-ui';
import { sx } from '../../componentsUi/Sx';

export const SidebarWrapper = styled(Box)([
  css({
    position: 'fixed',
    left: 0,
    bg: 'white',
    width: '150px',
    color: 'text',
    height: '100%',
    borderRadiusTopRight: '30px',
    borderRadiusBottomRight: '30px',
    zIndex: 1,
  }),
  sx,
]);

export const SidebarLogo = styled('a')([
  css({
    display: 'block',
    cursor: 'pointer',
    width: '54px',
    height: '58px',
    backgroundSize: '100% auto',
    backgroundAttachment: 'scroll',
    backgroundRepeat: 'no-repeat',
    textIndent: '-9999px',
    backgroundImage: 'url(/media/logo-small.svg)',
  }),
  sx,
]);

export const SidebarLogoWrapper = styled(Box)([
  css({
    p: '50px',
    width: '192px',
    height: '225px',
    backgroundAttachment: 'scroll',
    backgroundRepeat: 'no-repeat',
    backgroundColor: 'darkblue',
    backgroundPosition: 'right bottom',
    backgroundImage: 'url(/media/sidebar-bg.svg)',
  }),
  sx,
]);

export const SidebarMenu = styled(Flex)([
  css({
    flexDirection: 'column',
    alignContent: 'space-around',
    listStyle: 'none',
    padding: 0,
    mt: 4,
    color: 'primary',
  }),
  sx,
]);

export const SidebarMenuWrapper = styled(Flex)([
  css({
    flexDirection: 'column',
    justifyContent: 'flex-start',
    height: 'calc(100vw - 99px)',
  }),
  sx,
]);

export const SideBarMenuItem = styled('a')([
  css({
    cursor: 'pointer',
    background: 'none',
    border: 'none',
    my: 4,
    mx: 5,
    p: 0,
  }),
  sx,
]);
