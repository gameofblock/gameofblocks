/** @jsx jsx */
import { FC, ReactNode } from 'react';
import Link from 'next/link';
import { Button, Flex, Image, jsx } from 'theme-ui';

import { User } from '../../types/user';
import { Head } from '../Head';

interface LayoutProps {
  children: ReactNode;
  title?: string;
  user?: User;
}

const Layout: FC<LayoutProps> = ({ children, title, user }: LayoutProps) => (
  <>
    <Head title={title} />
    <header
      sx={{
        p: 4,
        position: 'absolute',
        top: 0,
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        variant: 'styles.header',
      }}
    >
      <Link href="/" passHref>
        <a href="/">
          <Image
            sx={{ width: '350px' }}
            src="https://www.gameofblocks.io/static/media/logo.6a924c87.png"
          />
        </a>
      </Link>
      <div sx={{ mx: 'auto' }} />
      <Flex sx={{ ml: 'auto' }}>
        {!user ? (
          <Link href="/api/login" passHref>
            <Button as="a" variant="secondary">
              login
            </Button>
          </Link>
        ) : (
          <>
            <Link href="/api/logout" passHref>
              <a
                href="/"
                sx={{
                  variant: 'styles.a',
                  p: 2,
                }}
              >
                logout
              </a>
            </Link>
            <Link href="/profile" passHref>
              <a
                href="/"
                sx={{
                  variant: 'styles.a',
                  p: 2,
                }}
              >
                account
              </a>
            </Link>
          </>
        )}
      </Flex>
    </header>
    <main>{children}</main>
    <footer />
  </>
);

export { Layout };
