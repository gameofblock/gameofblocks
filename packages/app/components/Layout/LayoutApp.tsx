import React, { FC, ReactNode } from 'react';
import { User } from '../../types/user';
import { Head } from '../Head';
import {
  LayoutAppWrapper,
  LayoutHeader,
  LayoutContent,
  LayoutContentWrapper,
} from './Style';
import { Sidebar } from '../Sidebar';
import { Menu } from '../Menu';

interface LayoutAppProps {
  children: ReactNode;
  title?: string;
  user?: User;
}

const LayoutApp: FC<LayoutAppProps> = (props: LayoutAppProps) => {
  const { children, title, user } = props;
  return (
    <LayoutAppWrapper>
      <Head title={title} />
      <Sidebar user={user}>Sidebar</Sidebar>
      <LayoutContent>
        <LayoutHeader>
          <Menu />
        </LayoutHeader>
        <LayoutContentWrapper>{children}</LayoutContentWrapper>
      </LayoutContent>
    </LayoutAppWrapper>
  );
};

export { LayoutApp };
