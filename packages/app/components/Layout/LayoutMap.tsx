import React, { FC, ReactNode } from 'react';
import { User } from '../../types/user';
import { Head } from '../Head';
import { LayoutAppWrapper } from './Style';

interface LayoutAppProps {
  children: ReactNode;
  title?: string;
  user?: User;
}

const LayoutApp: FC<LayoutAppProps> = (props: LayoutAppProps) => {
  const { children, title } = props;
  return (
    <LayoutAppWrapper>
      <Head title={title} />
      {children}
    </LayoutAppWrapper>
  );
};

export { LayoutApp };
