import styled from '@emotion/styled';
import { css, Box } from 'theme-ui';
import { sx } from '../../componentsUi/Sx';

export const LayoutAppWrapper = styled(Box)([
  css({
    minHeight: '100%',
  }),
  sx,
]);

export const LayoutContent = styled(Box)([
  css({
    pl: '150px',
  }),
  sx,
]);

export const LayoutHeader = styled(Box)([
  css({
    position: 'fixed',
    width: 'calc(100vw - 150px)',
    height: '225px',
    bg: 'darkblue',
    right: 0,
  }),
  sx,
]);

export const LayoutContentWrapper = styled(Box)([
  css({
    position: 'relative',
    zIndex: 10,
    maxWidth: '1140px',
    width: 'calc(100vw - 150px)',
    mx: 'auto',
    pt: '7',
    px: 3,
  }),
  sx,
]);
