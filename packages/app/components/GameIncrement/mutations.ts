import gql from 'graphql-tag';

import { GameEvent } from '../../types/game';

export interface InsertGameEventMutationVariables {
  data: any;
  gameId: string | string[];
  type: string;
  userId: string;
}

export interface InsertGameEventMutationResult {
  insert_game_events_one: GameEvent;
}

export const INSERT_GAME_EVENT = gql`
  mutation InsertGameEvent(
    $data: json
    $gameId: uuid
    $type: String
    $userId: uuid
  ) {
    insert_game_events_one(
      object: { data: $data, game_id: $gameId, type: $type, user_id: $userId }
    ) {
      data
      game_id
      id
      type
      user_id
    }
  }
`;
