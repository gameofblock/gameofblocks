import React, { FunctionComponent } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { Button, Box } from 'theme-ui';

import {
  INSERT_GAME_EVENT,
  InsertGameEventMutationResult,
  InsertGameEventMutationVariables,
} from './mutations';

interface GameIncrementProps {
  userId: string;
  id: string | string[];
}

const GameIncrement: FunctionComponent<GameIncrementProps> = (
  props: GameIncrementProps
) => {
  const { userId, id } = props;

  const [InsertGameEvent, { loading, error }] = useMutation<
    InsertGameEventMutationResult,
    InsertGameEventMutationVariables
  >(INSERT_GAME_EVENT, {
    onCompleted: async ({ insert_game_events_one: gameEvent }) => {
      console.log(`increment turn${gameEvent}`);
    },
  });

  if (loading) {
    return <Box>loading</Box>;
  }
  if (error) {
    return <Box>error</Box>;
  }
  return (
    <Button
      sx={{ mt: 2 }}
      onClick={async (): Promise<void> => {
        await InsertGameEvent({
          variables: {
            data: {},
            gameId: id,
            userId,
            type: 'INCREMENT_TURN',
          },
        });
      }}
    >
      nextTurn
    </Button>
  );
};

export { GameIncrement };
