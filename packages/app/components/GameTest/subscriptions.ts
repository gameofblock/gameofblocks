import gql from 'graphql-tag';

export const GAME = gql`
  subscription Game($id: uuid) {
    games(where: { id: { _eq: $id } }) {
      currentTurn: current_turn
      currentPlayerId: current_player_id
      currentPlayer: current_player
      id
    }
  }
`;
