import React, { FC } from 'react';
import { Box } from 'theme-ui';
import { useSubscription } from '@apollo/react-hooks';

import { GAME } from './subscriptions';
import { GameIncrement } from '../GameIncrement';

interface Props {
  id: string | string[];
  userId: string;
}

const GameTest: FC<Props> = ({ id, userId }: Props) => {
  const { data, error, loading } = useSubscription(GAME, {
    variables: { id },
  });

  if (loading) {
    return <Box>Chargement...</Box>;
  }

  if (error) {
    return <Box>error...</Box>;
  }

  const [game] = data.games;
  const { currentTurn, currentPlayer, currentPlayerId } = game;
  return (
    <Box sx={{ p: 2 }}>
      <Box>
        turn:
        {currentTurn}
      </Box>
      <Box>
        player number:
        {currentPlayer}
      </Box>
      <Box>
        current player id:
        {currentPlayerId}
      </Box>
      <GameIncrement id={id} userId={userId} />
    </Box>
  );
};

export { GameTest };
