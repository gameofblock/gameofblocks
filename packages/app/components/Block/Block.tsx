import React, { ReactElement, FunctionComponent } from 'react';
import { Box } from 'theme-ui';
import { motion } from 'framer-motion';
import { rgba } from 'polished';
import { BlockSvg, BlockImage } from './Style';
import theme from '../../theme/map';

const blockHeight = 200;
const blockWidth = 178;

const COLORS = [
  theme.colors.water,
  theme.colors.forest,
  theme.colors.desert,
  theme.colors.plain,
  theme.colors.mountain,
];

const image = {
  hidden: {
    opacity: 0,
  },
  visible: {
    opacity: 1,
  },
};

const block = {
  hidden: ({ startColor }): any => {
    return {
      opacity: 0,
      pathLength: 0,
      fill: startColor,
    };
  },
  visible: ({ endColor }): any => ({
    opacity: 1,
    pathLength: 1,
    fill: endColor,
  }),
};

interface BlockProps {
  x: number;
  y: number;
}

const Block: FunctionComponent<BlockProps> = ({
  x,
  y,
}: BlockProps): ReactElement => {
  const randomDelay = (Math.random() * (5 - 0.2) + 0.3).toFixed(4);
  const imageDelay = parseFloat(randomDelay) + 1.5;
  const imageType = Math.floor(Math.random() * 5);
  const color = COLORS[imageType];
  const startColor = rgba(COLORS[imageType], 0);
  const endColor = rgba(COLORS[imageType], 1);
  return (
    <Box
      key={x + y}
      sx={{
        left: x,
        top: y,
        width: blockWidth,
        height: blockHeight,
        position: 'absolute',
      }}
    >
      <motion.div
        initial="hidden"
        animate="visible"
        transition={{
          default: {
            delay: imageDelay,
            duration: 1,
            ease: 'easeInOut',
          },
        }}
        variants={image}
      >
        <BlockImage type={imageType} />
      </motion.div>
      <BlockSvg
        custom={{ startColor, endColor }}
        color={color}
        viewBox="0 0 178 201"
      >
        <motion.path
          initial="hidden"
          animate="visible"
          variants={block}
          custom={{ startColor, endColor }}
          transition={{
            default: {
              delay: randomDelay,
              duration: 1,
              ease: 'easeInOut',
            },
            fill: {
              delay: randomDelay,
              duration: 2,
              ease: [1, 0, 0.8, 1],
            },
          }}
          d="M170.598916,45.5689456 L170.598864,45.5689159 L96.0638593,2.79569826 C91.5550096,0.557204839 86.35541,0.557204839 81.8733069,2.78139234 L7.40090586,45.5689456 C3.5722671,47.7654157 0.499911,53.2565759 0.499911,57.8294336 L0.49981725,143.394541 C0.587894104,147.942172 3.70593837,153.493638 7.49035996,155.66474 L82.025365,198.437958 C86.5336295,200.676161 91.8210634,200.676161 96.2156925,198.452393 L170.598916,155.664711 C174.427555,153.468241 177.499911,147.97708 177.499911,143.404223 L177.499911,57.8294336 C177.499911,53.3141013 174.402193,47.7508656 170.598916,45.5689456 Z"
        />
      </BlockSvg>
    </Box>
  );
};

export { Block };
