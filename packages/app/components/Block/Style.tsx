import styled from '@emotion/styled';
import { css, Box } from 'theme-ui';
import { motion, SVGMotionProps } from 'framer-motion';
import { sx } from '../../componentsUi/Sx';
import water from '../../public/media/block/block-water.png';
import forest from '../../public/media/block/block-forest.png';
import desert from '../../public/media/block/block-desert.png';
import plain from '../../public/media/block/block-plain.png';
import mountain from '../../public/media/block/block-mountain.png';

const IMAGES = [water, forest, desert, plain, mountain];

interface BlockSVGProps extends SVGMotionProps<any> {
  color?: string;
}

export const BlockSvg = styled(motion.svg)([
  ({ color }: BlockSVGProps): any => {
    return css({
      stroke: color,
      strokeWidth: 1,
      strokeLinejoin: 'round',
      strokeLinecap: 'round',
    });
  },
  sx,
]);

interface BlockImageProps {
  type: number;
}

export const BlockImage = styled(Box)([
  ({ type }: BlockImageProps): any => {
    return css({
      width: '156px',
      height: '85px',
      backgroundImage: `url(${IMAGES[type]})`,
      backgroundSize: '100%',
      position: 'absolute',
      top: '11px',
      left: '11px',
    });
  },
  sx,
]);
