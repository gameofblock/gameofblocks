import React, { FC } from 'react';
import NextHead from 'next/head';

interface HeadProps {
  title?: string;
}

const Head: FC<HeadProps> = ({ title }: HeadProps) => (
  <NextHead>
    <meta charSet="UTF-8" />
    <title>
      Game of Blocks
      {`- ${title}` || ''}
    </title>
    <meta name="language" content="en-EN" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" sizes="192x192" href="/static/touch-icon.png" />
    <link rel="apple-touch-icon" href="/static/touch-icon.png" />
    <link rel="mask-icon" href="/static/favicon-mask.svg" color="#49B882" />
    <link rel="icon" href="/favicon.ico" />
    {/* @TODO: make all exteral format  */}
    <meta name="twitter:site" content="" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:title" content="Game of Blocks" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:image:width" content="600" />
    <meta property="og:image:height" content="600" />
  </NextHead>
);

export { Head };
