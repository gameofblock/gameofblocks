import gql from 'graphql-tag';

import { Game } from '../../types/game';

export interface CreateGameMutationVariables {
  prize: string;
  maxPlayer: string;
  turnNumber: string;
  xSize: string;
  ySize: string;
  players: any;
  currentPlayerId: string;
  gameUsers: { user_id: string }[];
}

export interface CreateGameMutationResult {
  insert_games_one: Game;
}

export const CREATE_GAME = gql`
  mutation create_game(
    $prize: numeric!
    $maxPlayer: Int!
    $turnNumber: Int!
    $xSize: Int!
    $ySize: Int!
    $players: json
    $currentPlayerId: String
    $gameUsers: [game_user_insert_input!]!
  ) {
    insert_games_one(
      object: {
        prize: $prize
        y_size: $ySize
        x_size: $xSize
        turn_number: $turnNumber
        max_players: $maxPlayer
        players: $players
        current_player_id: $currentPlayerId
        game_users: { data: $gameUsers }
      }
    ) {
      id
      max_players
      prize
      players
      turn_number
      x_size
      y_size
      game_users {
        game_id
        id
        user_id
      }
    }
  }
`;
