import * as Yup from 'yup';

export default Yup.object().shape({
  prize: Yup.string().required('required'),
  maxPlayer: Yup.string().required('required'),
  turnNumber: Yup.string().required('required'),
  xSize: Yup.string().required('required'),
  ySize: Yup.string().required('required'),
});
