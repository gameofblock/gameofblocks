/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { FunctionComponent } from 'react';
import { Button, Text, Input, Label } from 'theme-ui';
import { useFormik } from 'formik';

import createGameValidationSchema from './createGameValidationSchema';

export interface SubmitProps {
  prize: string;
  maxPlayer: string;
  turnNumber: string;
  xSize: string;
  ySize: string;
}

interface CreateGameFormProps {
  loading: boolean;
  hasError: boolean;
  onSubmit: (props: SubmitProps) => Promise<void>;
}

export const CreateGameForm: FunctionComponent<CreateGameFormProps> = (
  props: CreateGameFormProps
) => {
  const { hasError, onSubmit, loading } = props;
  const { errors, handleChange, values, handleSubmit } = useFormik({
    initialValues: {
      prize: '10',
      maxPlayer: '4',
      turnNumber: '10',
      xSize: '10',
      ySize: '10',
    },
    validationSchema: createGameValidationSchema,
    onSubmit: async (
      { prize, maxPlayer, turnNumber, xSize, ySize },
      { setSubmitting }
    ) => {
      await onSubmit({
        prize,
        maxPlayer,
        turnNumber,
        xSize,
        ySize,
      });
      setSubmitting(false);
    },
  });

  return (
    <form onSubmit={handleSubmit}>
      <Label mb={2} htmlFor="name">
        Name
      </Label>
      <Input
        required
        id="prize"
        name="prize"
        type="text"
        placeholder="Prize"
        sx={{ mb: 1 }}
        onChange={handleChange}
        value={values.prize}
      />
      {errors.prize && <Text>{errors.prize}</Text>}
      <Input
        required
        id="maxPlayer"
        name="maxPlayer"
        placeholder="Max player"
        type="text"
        sx={{ mb: 1 }}
        onChange={handleChange}
        value={values.maxPlayer}
      />
      {errors.maxPlayer && <Text>{errors.maxPlayer}</Text>}
      <Input
        required
        id="turnNumber"
        name="turnNumber"
        placeholder="max turn"
        type="text"
        sx={{ mb: 1 }}
        onChange={handleChange}
        value={values.turnNumber}
      />
      {errors.turnNumber && <Text>{errors.turnNumber}</Text>}
      <Input
        required
        id="xSize"
        name="xSize"
        placeholder="x size"
        type="text"
        sx={{ mb: 1 }}
        onChange={handleChange}
        value={values.xSize}
      />
      {errors.xSize && <Text>{errors.xSize}</Text>}
      <Input
        required
        id="ySize"
        name="ySize"
        placeholder="y size"
        type="text"
        sx={{ mb: 1 }}
        onChange={handleChange}
        value={values.ySize}
      />
      {errors.ySize && <Text>{errors.ySize}</Text>}

      {!loading && hasError && (
        <Text color="red">Oops something went wrong</Text>
      )}

      <Button disabled={loading} mt={2} type="submit">
        Submit
      </Button>
    </form>
  );
};

export default CreateGameForm;
