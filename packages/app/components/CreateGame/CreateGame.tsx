import React, { FunctionComponent } from 'react';
import { useRouter } from 'next/router';
import { useMutation } from 'react-apollo';
import { Heading } from 'theme-ui';
import { Container } from '../../componentsUi/container';

import { CreateGameForm } from './CreateGameForm';
import {
  CREATE_GAME,
  CreateGameMutationResult,
  CreateGameMutationVariables,
} from './mutations';

interface CreateGameProps {
  userId: string;
}

const fakeUsers = [
  { user_id: 'auth0|5e99639a3f2efd0c0732c52c' },
  { user_id: 'auth0|5ebf1c3d38aa9b0be75a43cf' },
];

export const CreateGame: FunctionComponent<CreateGameProps> = (
  props: CreateGameProps
) => {
  const { userId } = props;
  const router = useRouter();
  const [createGame, { loading, error }] = useMutation<
    CreateGameMutationResult,
    CreateGameMutationVariables
  >(CREATE_GAME, {
    onCompleted: async ({ insert_games_one: game }) => {
      await router.push('/games/[id]', `/games/${game.id}`);
    },
  });

  const submit = async ({
    prize,
    maxPlayer,
    turnNumber,
    xSize,
    ySize,
  }): Promise<void> => {
    await createGame({
      variables: {
        prize,
        maxPlayer,
        turnNumber,
        xSize,
        ySize,
        currentPlayerId: 'auth0|5e99639a3f2efd0c0732c52c',
        players: [
          'auth0|5e99639a3f2efd0c0732c52c',
          'auth0|5ebf1c3d38aa9b0be75a43cf',
          'auth0|5e99639a3f2efd0c0732c52c',
          'auth0|5ebf1c3d38aa9b0be75a43cf',
        ],
        gameUsers: fakeUsers,
      },
    });
  };

  return (
    <Container>
      <Heading sx={{ mb: '4' }}>Create a new game</Heading>
      <CreateGameForm hasError={!!error} loading={loading} onSubmit={submit} />
    </Container>
  );
};

export default CreateGame;
