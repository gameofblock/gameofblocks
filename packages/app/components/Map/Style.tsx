import styled from '@emotion/styled';
import { css, Box } from 'theme-ui';
import { motion } from 'framer-motion';
import { sx } from '../../componentsUi/Sx';

interface MapWrapperProps {
  gridHeight: number;
  gridWidth: number;
  mapOffsetX: number;
  mapOffsetY: number;
}

export const MapContainer = styled(motion.div)([
  ({ gridHeight, gridWidth, mapOffsetX, mapOffsetY }: MapWrapperProps): any => {
    return css({
      height: gridHeight,
      width: gridWidth,
      left: mapOffsetX,
      top: mapOffsetY,
      bg: 'dark',
      position: 'fixed',
    });
  },
  sx,
]);

export const MapWrapper = styled(Box)([
  css({
    bg: 'dark',
    display: 'flex',
    placeContent: 'center center',
  }),
  sx,
]);
