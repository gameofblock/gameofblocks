import React, { ReactElement } from 'react';
import { defineGrid, extendHex } from 'honeycomb-grid';
import { useWindowSize } from '@react-hook/window-size';
import { MapWrapper, MapContainer } from './Style';
import { Block } from '../Block';

// const borderWidth = 6;
// const radius = 100;
const mapSizeY = 10;
const mapSizeX = 15;

function Map(): ReactElement {
  // Get window size on UseEffect
  const [width, height] = useWindowSize();

  // Create a custom Hex block
  const Hex = extendHex({
    // size: radius + borderWidth,
    size: { height: 212, width: 183 },
  });
  // Define a grid
  const Grid = defineGrid(Hex);
  const grid = Grid.rectangle({
    width: mapSizeX,
    height: mapSizeY,
  });
  // Get the global size of the grid
  const gridHeight = grid.pointHeight();
  const gridWidth = grid.pointWidth();
  // Get the offset to center the map & create drag limit
  // TODO: add condition where window is bigger than map in x or y
  const mapOffsetX = -(gridWidth - width) / 2;
  const mapOffsetY = -(gridHeight - height) / 2;

  return (
    <MapWrapper>
      <MapContainer
        drag
        dragConstraints={{
          left: mapOffsetX,
          right: -mapOffsetX,
          top: mapOffsetY,
          bottom: -mapOffsetY,
        }}
        mapOffsetX={mapOffsetX}
        mapOffsetY={mapOffsetY}
        gridHeight={gridHeight}
        gridWidth={gridWidth}
      >
        {grid.map((hex) => {
          const { x, y } = hex.toPoint();
          return <Block key={x + y} x={x} y={y} />;
        })}
      </MapContainer>
    </MapWrapper>
  );
}

export { Map };
