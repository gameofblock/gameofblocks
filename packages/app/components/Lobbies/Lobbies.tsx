import React, { FunctionComponent } from 'react';
import { Box, Heading } from 'theme-ui';
import { useSubscription } from '@apollo/react-hooks';
import { LOBBIES } from './subscriptions';
import { Container } from '../../componentsUi/container';

const Lobbies: FunctionComponent = () => {
  const { data, error, loading } = useSubscription(LOBBIES);

  if (loading) {
    return <Box>Chargement...</Box>;
  }

  if (error) {
    return <Box>error...</Box>;
  }

  return (
    <Container>
      <Heading sx={{ mb: '4' }}>Lobbie list</Heading>
      <Box>
        {data.lobbies.map((lobby) => {
          return <Box key={lobby.id}>{lobby.name}</Box>;
        })}
      </Box>
    </Container>
  );
};

export { Lobbies };
