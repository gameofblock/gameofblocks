import gql from 'graphql-tag';

export const LOBBIES = gql`
  subscription Lobbies {
    lobbies {
      id
      name
    }
  }
`;
