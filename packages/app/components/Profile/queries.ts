import gql from 'graphql-tag';

export interface UsersQueryProps {
  users: {
    picture: string;
    email: string;
  }[];
}

export const USERS = gql`
  query users {
    users {
      picture
      email
    }
  }
`;
