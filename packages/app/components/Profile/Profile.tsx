import React, { FunctionComponent } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { Image, Heading } from 'theme-ui';

import { Container } from '../../componentsUi/container';

import { USERS, UsersQueryProps } from './queries';

export const Profile: FunctionComponent = () => {
  const { data, error, loading } = useQuery<UsersQueryProps>(USERS);
  if (error) {
    return <div>{error.message}</div>;
  }

  if (loading) {
    return <div>Chargement...</div>;
  }

  const [user] = data.users;
  return (
    <Container>
      <Heading sx={{ mb: '4' }}>Your profile</Heading>
      {user.picture && (
        <Image src={user.picture} alt="avatar" variant="avatar" />
      )}
      <div>{`Email: ${user.email}`}</div>
    </Container>
  );
};

export default Profile;
