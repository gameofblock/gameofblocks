import { initAuth0 } from '@auth0/nextjs-auth0';
import getConfig from 'next/config';

// Only holds serverRuntimeConfig and publicRuntimeConfig
const { publicRuntimeConfig } = getConfig();

export default initAuth0({
  clientId: publicRuntimeConfig.AUTH0_CLIENT_ID,
  clientSecret: publicRuntimeConfig.AUTH0_CLIENT_SECRET,
  scope: 'openid profile email',
  audience: 'hasura',
  domain: publicRuntimeConfig.AUTH0_DOMAIN,
  redirectUri: publicRuntimeConfig.REDIRECT_URI,
  postLogoutRedirectUri: publicRuntimeConfig.POST_LOGOUT_REDIRECT_URI,
  session: {
    cookieSecret:
      publicRuntimeConfig.SESSION_COOKIE_SECRET ||
      'H1A9YQxjzfBjroheQ56GuK57II99sVpv',
    cookieLifetime: publicRuntimeConfig.SESSION_COOKIE_LIFETIME,
    // Store the id_token in the session. Defaults to false.
    storeIdToken: true,
    // Store the access_token in the session. Defaults to false.
    storeAccessToken: false,
    // Store the refresh_token in the session. Defaults to false.
    storeRefreshToken: true,
  },
});
