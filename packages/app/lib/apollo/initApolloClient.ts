import { ApolloClient } from 'apollo-client';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import getConfig from 'next/config';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { split } from 'apollo-link';

// Only holds serverRuntimeConfig and publicRuntimeConfig
const { publicRuntimeConfig } = getConfig();

let apolloClient: ApolloClient<NormalizedCacheObject> = null;
const isBrowser = typeof window !== 'undefined';
/**
 * Creates and configures the ApolloClient
 * @param  {Object} [initialState={}]
 */

function createApolloClient(
  initialState = {},
  session = null,
  secret = null
): ApolloClient<NormalizedCacheObject> {
  // Check out https://github.com/zeit/next.js/pull/4611 if you want to use the AWSAppSyncClient
  const anonymousHeader = { 'x-hasura-role': 'anonymous' };
  const secretHeader = secret
    ? { 'x-hasura-admin-secret': secret }
    : anonymousHeader;

  const sessionHeader = session
    ? {
        authorization: `Bearer ${session.idToken}`,
        'x-hasura-role':
          session.user['https://hasura.io/jwt/claims']['x-hasura-default-role'],
      }
    : anonymousHeader;

  const headers = secret ? secretHeader : sessionHeader;
  const graphqlServerUri = publicRuntimeConfig.GRAPHQL_SERVER_URI;

  const httpLink = new HttpLink({
    uri: graphqlServerUri,
    credentials: 'include',
    fetch,
    headers,
  });

  console.log(
    `=> graphqlServerUri: ${graphqlServerUri} , isBrowser: ${isBrowser}`
  );

  // using the ability to split links, you can send data to each link
  // depending on what kind of operation is being sent
  const link = !isBrowser
    ? httpLink
    : split(
        // split based on operation type
        ({ query }) => {
          const definition = getMainDefinition(query);
          return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
          );
        },
        new WebSocketLink({
          uri: publicRuntimeConfig.GRAPHQL_SERVER_WS_URI,
          options: {
            reconnect: true,
            connectionParams: {
              headers,
            },
          },
        }),
        httpLink
      );

  return new ApolloClient({
    ssrMode: typeof window === 'undefined', // Disables forceFetch on the server (so queries are only run once)
    link,
    cache: new InMemoryCache().restore(initialState),
  });
}

interface ApolloClientProps {
  initialState?: any;
  session?: any;
  secret?: any;
}

/**
 * Always creates a new apollo client on the server
 * Creates or reuses apollo client in the browser.
 * @param  {Object} initialState
 */
export function initApolloClient({
  initialState = {},
  session = null,
  secret = null,
}: ApolloClientProps): ApolloClient<NormalizedCacheObject> {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (typeof window === 'undefined') {
    return createApolloClient(initialState, session, secret);
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = createApolloClient(initialState, session, secret);
  }

  return apolloClient;
}

export default initApolloClient;
