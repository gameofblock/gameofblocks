import ApolloClient from 'apollo-client';
import { NextPageContext } from 'next';

export interface WithApolloOptions {
  ssr: boolean;
}

export interface WithApolloState<TCache> {
  data?: TCache;
}

export interface WithApolloProps<TCache> {
  apolloState: WithApolloState<TCache>;
  apollo: ApolloClient<TCache>;
}

export interface ApolloPageContext<C = any> extends NextPageContext {
  // Custom prop added by withApollo
  apolloClient: ApolloClient<C>;
}
