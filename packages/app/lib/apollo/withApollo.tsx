/* eslint-disable react/jsx-props-no-spreading */
import { NextPage } from 'next';
import React, { ReactElement } from 'react';
import { ApolloProvider } from '@apollo/react-hooks';
import Head from 'next/head';
import { ISession } from '@auth0/nextjs-auth0/dist/session/session';
import ApolloClient from 'apollo-client';

import { initApolloClient } from './initApolloClient';
import { WithApolloOptions, WithApolloProps, ApolloPageContext } from './types';
import { logger } from '../../utils/logger';

import auth0 from '../auth0';
import { USERS, UsersQueryProps } from './queries';
import { User } from '../../types/user';

interface ApolloProps<P> extends WithApolloProps<P> {
  apolloClient: any;
  session: any;
}

async function getUser(apolloClient: ApolloClient<any>): Promise<User> {
  try {
    const { data } = await apolloClient.query<UsersQueryProps>({
      query: USERS,
    });
    const [user] = data.users;
    return user;
  } catch (err) {
    return null;
  }
}

const withApollo = <P extends object>(
  PageComponent: NextPage<P>,
  options: WithApolloOptions = { ssr: true }
): any => {
  const { ssr } = options;
  const WithApollo = (props: ApolloProps<P>): ReactElement => {
    const { apolloState, session, ...pageProps } = props;
    const client =
      pageProps.apolloClient ||
      initApolloClient({ initialState: apolloState, session });
    return (
      <ApolloProvider client={client}>
        <PageComponent {...(pageProps as P)} />
      </ApolloProvider>
    );
  };

  // Set the correct displayName in development
  if (process.env.NODE_ENV !== 'production') {
    const displayName =
      PageComponent.displayName || PageComponent.name || 'Component';

    if (displayName === 'App') {
      logger.warn('This withApollo HOC only works with PageComponents.');
    }

    WithApollo.displayName = `withApollo(${displayName})`;
  }

  if (ssr || PageComponent.getInitialProps) {
    WithApollo.getInitialProps = async (
      ctx: ApolloPageContext
    ): Promise<any> => {
      const { AppTree } = ctx;

      let session: ISession;
      if (typeof window === 'undefined') {
        session = await auth0.getSession(ctx.req);
      } else {
        const res = await fetch('/api/session', {});
        session = await res.json();
      }
      // Initialize ApolloClient, add it to the ctx object so
      // we can use it in `PageComponent.getInitialProp`.
      ctx.apolloClient = initApolloClient({
        initialState: {},
        session,
      });

      const { apolloClient } = ctx;
      // Run wrapped getInitialProps methods
      let pageProps: { user?: User } = {};

      if (PageComponent.getInitialProps) {
        pageProps = await PageComponent.getInitialProps(ctx);
      }

      // Inject user data to page props
      if (session || typeof window !== 'undefined') {
        pageProps.user = await getUser(apolloClient);
      }

      // Only on the server:
      if (typeof window === 'undefined') {
        // When redirecting, the response is finished.
        // No point in continuing to render
        if (ctx.res && ctx.res.finished) {
          return pageProps;
        }

        // Only if ssr is enabled
        if (ssr) {
          try {
            // Run all GraphQL queries
            const { getDataFromTree } = await import('@apollo/react-ssr');
            await getDataFromTree(
              <AppTree
                pageProps={{
                  ...pageProps,
                  apolloClient,
                  session,
                }}
              />
            );
          } catch (error) {
            // Prevent Apollo Client GraphQL errors from crashing SSR.
            // Handle them in components via the data.error prop:
            // https://www.apollographql.com/docs/react/api/react-apollo.html#graphql-query-data-error
            logger.error('Error while running `getDataFromTree`', error);
          }

          // getDataFromTree does not call componentWillUnmount
          // head side effect therefore need to be cleared manually
          Head.rewind();
        }
      }

      // Extract query data from the Apollo store
      const apolloState = apolloClient.cache.extract();

      return {
        ...pageProps,
        apolloState,
        session,
      };
    };
  }

  return WithApollo;
};

export default withApollo;
