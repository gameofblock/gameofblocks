import gql from 'graphql-tag';

import { User } from '../../types/user';

export interface UsersQueryProps {
  users: User[];
}

export const USERS = gql`
  query users {
    users {
      id
      auth_id
      picture
      email
    }
  }
`;
