/* eslint-disable react/jsx-props-no-spreading */
import React, { ReactElement } from 'react';
import { NextPage, NextPageContext } from 'next';
import { User } from '../types/user';

const isBrowser = typeof window !== 'undefined';
const withAuthSync = (PageComponent: NextPage): any => {
  const WithAuthSync = (props): ReactElement => {
    return <PageComponent {...props} />;
  };
  WithAuthSync.getInitialProps = async (ctx: NextPageContext): Promise<any> => {
    let pageProps: { user?: User } = {};
    pageProps = await PageComponent.getInitialProps(ctx);
    if (!pageProps.user && isBrowser) {
      window.location.replace('/');
    }
    if (!pageProps.user && !isBrowser) {
      ctx.res.writeHead(302, { Location: '/' });
      ctx.res.end();
    }
    return {
      ...pageProps,
    };
  };

  return WithAuthSync;
};

export default withAuthSync;
