const env = require('@gameofblocks/env');
const webpack = require('webpack');
const withOptimizedImages = require('next-optimized-images');

module.exports = withOptimizedImages({
  serverRuntimeConfig: {
    HASURA_SECRET: env.HASURA_SECRET || 'secret',
  },
  handleImages: ['jpeg', 'png', 'webp', 'gif'],
  publicRuntimeConfig: {
    AUTH0_DOMAIN: env.AUTH0_DOMAIN,
    AUTH0_CLIENT_ID: env.AUTH0_CLIENT_ID,
    AUTH0_CLIENT_SECRET: env.AUTH0_CLIENT_SECRET,
    AUTH0_SCOPE: 'openid profile offline_access',
    SESSION_COOKIE_SECRET: env.SESSION_COOKIE_SECRET,
    SECURE_COOKIE: env.SECURE_COOKIE,
    GRAPHQL_SERVER_WS_URI:
      env.GRAPHQL_SERVER_WS_URI || 'ws://localhost:5000/v1/graphql',
    GRAPHQL_SERVER_URI:
      env.GRAPHQL_SERVER_URI || 'http://localhost:5000/v1/graphql',
    REDIRECT_URI:
      env.AUTH0_CALLBACK_URL || 'http://localhost:3000/api/callback',
    POST_LOGOUT_REDIRECT_URI:
      env.POST_LOGOUT_REDIRECT_URI || 'http://localhost:3000',
    SESSION_COOKIE_LIFETIME: 60 * 60 * 8, // 8 hours
    NODE_ENV: env.NODE_ENV,
  },
  webpack: (config) => {
    config.plugins.push(new webpack.EnvironmentPlugin(env));
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(js|ts)x?$/,
      },
      use: ['@svgr/webpack'],
    });
    return config;
  },
});
