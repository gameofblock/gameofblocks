import {
  createShouldForwardProp,
  props,
} from '@styled-system/should-forward-prop';

const forwardProps = (forwardedProps): void => {
  createShouldForwardProp([...props, forwardedProps]);
};

export { forwardProps };
