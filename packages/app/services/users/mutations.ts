import gql from 'graphql-tag';

export const CREATE_USER = gql`
  mutation create_user($authId: String!, $email: String, $picture: String) {
    insert_users_one(
      object: {
        email: $email
        picture: $picture
        active: true
        auth_id: $authId
      }
    ) {
      id
      active
      auth_id
      email
      last_login
      picture
      username
    }
  }
`;

export const UPDATE_LAST_LOGIN = gql`
  mutation update_user_last_login($authId: String!, $lastLogin: timestamptz!) {
    update_users(
      where: { auth_id: { _eq: $authId } }
      _set: { last_login: $lastLogin }
    ) {
      affected_rows
      returning {
        id
        last_login
      }
    }
  }
`;
