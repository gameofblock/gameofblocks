import getConfig from 'next/config';
import { initApolloClient } from '../../lib/apollo/initApolloClient';
import { GET_USER } from './queries';
import { CREATE_USER, UPDATE_LAST_LOGIN } from './mutations';
import {
  UserQueryResult,
  UserQueryVariables,
  CreateUserMutationResult,
  CreateUserVariables,
  UpdateLastLoginVariables,
  User,
  Auth0User,
} from './types';
import { logger } from '../../utils/logger';

const { serverRuntimeConfig } = getConfig();

const apolloClient = initApolloClient({
  initialState: {},
  session: null,
  secret: serverRuntimeConfig.HASURA_SECRET,
});

export async function find(authId: string): Promise<User> {
  logger.info(`Trying to match ${authId} in database...`);
  const { data } = await apolloClient.query<
    UserQueryResult,
    UserQueryVariables
  >({
    query: GET_USER,
    variables: { authId },
    fetchPolicy: 'no-cache',
  });
  return data ? data.users[0] : null;
}

async function create(userToCreate: Auth0User): Promise<User> {
  const { email, picture, auth_id: authId } = userToCreate;
  const { data, errors } = await apolloClient.mutate<
    CreateUserMutationResult,
    CreateUserVariables
  >({
    mutation: CREATE_USER,
    variables: { email, picture, authId },
  });

  if (errors) {
    logger.error(errors);
    return null;
  }

  const { insert_users_one: user } = data;
  return user;
}

async function updateLastLogin(authId: string): Promise<void> {
  await apolloClient.mutate<{}, UpdateLastLoginVariables>({
    mutation: UPDATE_LAST_LOGIN,
    variables: {
      authId,
      lastLogin: new Date().toISOString(),
    },
  });
}

export async function handleUserLogin(auth0User: Auth0User): Promise<User> {
  const { auth_id: authId } = auth0User;
  let user = await find(authId);
  if (!user) {
    logger.info(`🚫 User ${authId} does not exist. User creation attempt...`);
    user = await create(auth0User);
    logger.info(`User ${user.auth_id} created`);
  } else {
    logger.info(`👋 User ${authId} logged in successfully`);
    await updateLastLogin(authId);
  }
  return user;
}
