import { SerializedStyles } from '@emotion/core';
import { css } from 'theme-ui';

export const sx = (props): SerializedStyles =>
  props.sx && css(props.sx)(props.theme);
