import styled from '@emotion/styled';
import { css, Box } from 'theme-ui';
import { sx } from './Sx';

export const Container = styled(Box)([
  css({
    bg: 'background',
    boxShadow: ' 0 2px 24px 0 rgba(9,93,93,0.06), 0 2px 2px 0 rgba(0,0,0,0.03)',
    borderRadius: '13px',
    justifyContent: 'flex-end',
    padding: '4',
    zIndex: 1,
  }),
  sx,
]);
