import React, { FC, ReactElement } from 'react';
import styled from '@emotion/styled';
import { css } from 'theme-ui';
import { SerializedStyles } from '@emotion/core';

import Dashboard from '../public/media/icons/icon-dashboard.svg';
import Sword from '../public/media/icons/icon-sword.svg';
import Profile from '../public/media/icons/icon-profile.svg';
import Deck from '../public/media/icons/icon-deck.svg';
import Notification from '../public/media/icons/icon-notification.svg';
import Logout from '../public/media/icons/icon-logout.svg';

const ICONS = {
  dashboard: Dashboard,
  sword: Sword,
  profile: Profile,
  deck: Deck,
  notification: Notification,
  logout: Logout,
};

interface IconElementParams {
  name: string;
  size: string;
  color?: string;
}

function IconElement({
  name,
  size = '12',
  ...props
}: IconElementParams): ReactElement {
  return React.createElement(ICONS[name], {
    width: `${size}`,
    height: `${size}`,
    ...props,
  });
}

interface IconWrapperProps {
  hasLineWrapper: boolean;
}

const IconWrapperStyled = styled('i')([
  ({ hasLineWrapper }: IconWrapperProps): any => {
    return css({
      position: 'relative',
      display: 'inline-flex',
      alignSelf: 'center',
      top: hasLineWrapper ? '0.225em' : 0,
    });
  },
]);

interface StyleIconProps {
  color: string;
}

export const StyledIcon = styled(IconElement)([
  ({ color }: StyleIconProps): any => {
    return css({
      fill: color || null,
      verticalAlign: 'middle',
      path: { fill: color || null },
    });
  },
]);

interface IconProps {
  name: string;
  sx?: SerializedStyles;
  size?: string;
  color?: string;
  hasLineWrapper?: boolean;
}

const Icon: FC<IconProps> = ({
  name,
  size,
  color,
  hasLineWrapper,
}: IconProps) => {
  return (
    <IconWrapperStyled hasLineWrapper={hasLineWrapper}>
      <StyledIcon color={color} name={name} size={size} />
    </IconWrapperStyled>
  );
};

export default Icon;
