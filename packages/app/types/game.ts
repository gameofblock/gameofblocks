export interface Game {
  prize: string;
  maxPlayer: string;
  turnNumber: string;
  xSize: string;
  ySize: string;
  id: string;
}

export interface GameEvent {
  data: string;
  game_id: string;
  id: string;
  type: string;
  user_id: string;
}
