export interface UserType {
  displayName: string;
  id: string;
  user_id: string;
  picture: string;
  name: {
    value: string;
  }[];
  emails: {
    value: string;
  }[];
  nickname: string;
}

export interface User {
  id: string;
  picture: string;
  email: string;
}
