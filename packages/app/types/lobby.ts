export interface Lobby {
  id: string;
  created_at: string;
  name: string;
  user_id: string;
}
