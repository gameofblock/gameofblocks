import { NextPage } from 'next';
import { User } from './user';

export interface UserProps {
  user?: User;
}

export interface ErrorProps {
  statusCode: number;
}

export type AuthPage = NextPage<UserProps>;
