/* eslint-disable @typescript-eslint/camelcase */
import { NextApiRequest, NextApiResponse } from 'next';

import auth0 from '../../../lib/auth0';
import { logger } from '../../../utils/logger';
import { handleUserLogin } from '../../../services/users';

export default async function register(
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> {
  try {
    const session = await auth0.getSession(req);
    const { sub, email, nickname, picture } = session.user;
    await handleUserLogin({
      auth_id: sub,
      email,
      picture,
      nickname,
    });

    res.writeHead(302, {
      Location: '/dashboard',
    });
    res.end();
  } catch (error) {
    logger.error(error);
    res.status(error.status || 500).end(error.message);
  }
}
