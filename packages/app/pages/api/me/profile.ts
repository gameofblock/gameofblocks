import { NextApiRequest, NextApiResponse } from 'next';

import auth0 from '../../../lib/auth0';

export default async function profile(
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> {
  try {
    await auth0.handleProfile(req, res);
  } catch (error) {
    res.status(error.status || 500).end(error.message);
  }
}
