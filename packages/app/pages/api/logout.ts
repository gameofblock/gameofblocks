import auth0 from '../../lib/auth0';
import { logger } from '../../utils/logger';

export default async function logout(req, res): Promise<void> {
  try {
    await auth0.handleLogout(req, res);
  } catch (error) {
    logger.error(error);
    res.status(error.status || 500).end(error.message);
  }
}
