import { NextApiRequest, NextApiResponse } from 'next';

import { logger } from '../../utils/logger';

export default async function callback(
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> {
  try {
    res.status(200).json({ name: 'Next.js' });
  } catch (error) {
    logger.error(error);
    res.status(error.status || 500).end(error.message);
  }
}
