import React from 'react';
import Head from 'next/head';
import withAuthSync from '../lib/withAuthSync';
import { Profile } from '../components/Profile';
import { LayoutApp } from '../components/Layout';
import withApollo from '../lib/apollo/withApollo';
import { AuthPage, UserProps } from '../types/pages';

const ProfilePage: AuthPage = (props: UserProps) => {
  const { user } = props;
  return (
    <LayoutApp user={user}>
      <Head>
        <title>Game of blocks - Profile</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Profile />
    </LayoutApp>
  );
};

export default withAuthSync(withApollo(ProfilePage));
