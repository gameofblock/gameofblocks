import React from 'react';
import { useRouter } from 'next/router';
import { Text } from 'theme-ui';

import withAuthSync from '../../lib/withAuthSync';
import withApollo from '../../lib/apollo/withApollo';
import { LayoutApp } from '../../components/Layout';
import { AuthPage, UserProps } from '../../types/pages';

const LobbyPage: AuthPage = (props: UserProps) => {
  const { user } = props;
  const { query } = useRouter();
  return (
    <LayoutApp user={user}>
      <Text>{`Lobby ${query.id}`}</Text>
    </LayoutApp>
  );
};

export default withAuthSync(withApollo(LobbyPage));
