import React from 'react';

import withApollo from '../../lib/apollo/withApollo';
import withAuthSync from '../../lib/withAuthSync';
import { CreateLobby } from '../../components/CreateLobby';
import { LayoutApp } from '../../components/Layout';
import { UserProps, AuthPage } from '../../types/pages';

export const CreateLobbyPage: AuthPage = (props: UserProps) => {
  const { user } = props;
  return (
    <LayoutApp user={user}>
      <CreateLobby userId={user.id} />
    </LayoutApp>
  );
};

export default withAuthSync(withApollo(CreateLobbyPage));
