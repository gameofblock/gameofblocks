import React, { ReactElement } from 'react';
import dynamic from 'next/dynamic';

const Map = dynamic(() => import('../components/Map').then((mod) => mod.Map), {
  ssr: false,
});

function MapPage(): ReactElement {
  return (
    <div>
      <Map />
    </div>
  );
}

export default MapPage;
