/** @jsx jsx */
import Head from 'next/head';
import { Box, Button, Flex, Image, jsx, Text } from 'theme-ui';

import { Layout } from '../components/Layout';
import withApollo from '../lib/apollo/withApollo';
import { UserProps, AuthPage } from '../types/pages';

const Home: AuthPage = (props: UserProps) => {
  const { user } = props;
  return (
    <>
      <Head>
        <title>Game of blocks</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout title="homepage" user={user}>
        <Flex
          sx={{
            height: '100vh',
            minHeight: '915px',
            background: 'no-repeat -765px -745px url(/media/home-castle.svg)',
            backgroundAttachment: 'scroll',
            backgroundSize: '1380px auto',
          }}
        >
          <Flex
            sx={{ width: '55%', margin: 'auto', justifyContent: 'flex-start' }}
          >
            <Box sx={{ width: '45%', p: 2 }}>
              <Image src="/media/home-trone.png" />
            </Box>
            <Flex
              sx={{
                flexDirection: 'column',
                justifyContent: 'space-between',
                width: '55%',
              }}
            >
              <h1>Conquer blocks. Build your kingdom. Earn your keep.</h1>
              <Text variant="paragraph">
                A decentralized board game powered by Nebulas smart contracts
                that demands strategic skills, critical thinking, trading
                science.
              </Text>
              <Button as="a" sx={{ my: 4 }} variant="secondary">
                PLAY NOW!
              </Button>
            </Flex>
          </Flex>
        </Flex>
        <Flex
          sx={{
            height: '100vh',
            minHeight: '915px',
            background: '#282270 no-repeat bottom url(/media/mountain.png)',
            backgroundSize: '100% auto',
            justifyContent: 'fex-end',
          }}
        >
          <Flex
            sx={{
              flexDirection: 'column',
              width: '60%',
              mx: 'auto',
              color: 'text',
            }}
          >
            <h1 sx={{ p: 2, textAlign: 'center', color: 'text' }}>
              A strategic treasure conquest.
            </h1>
            <Box sx={{ width: '62%', mx: 'auto' }}>
              <Text sx={{ p: 2, textAlign: 'center' }} variant="paragraph">
                Plains, Forests, Mountains, Deserts, and Lake, The map is a
                beautiful island surrounded by frozen land.
              </Text>
              <Text sx={{ p: 2, textAlign: 'center' }} variant="paragraph">
                Build your kingdom block by block, but be sure to choose your
                land carefully and upgrade your kingdom wisely!
              </Text>
              <Text sx={{ p: 2, textAlign: 'center' }} variant="paragraph">
                Map holds a treasure chest. The treasure chest will be awarded
                to the most powerful kingdom at the end of each game.
              </Text>
            </Box>
          </Flex>
        </Flex>
      </Layout>
    </>
  );
};

export default withApollo(Home);
