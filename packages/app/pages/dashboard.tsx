import React from 'react';
import { NextPage } from 'next';
import Head from 'next/head';
import dynamic from 'next/dynamic';

import { LayoutApp } from '../components/Layout';
import withApollo from '../lib/apollo/withApollo';
import withAuthSync from '../lib/withAuthSync';
import { User } from '../types/user';

const Lobbies = dynamic(
  () => import('../components/Lobbies').then((mod) => mod.Lobbies),
  { ssr: false }
);

const DashboardPage: NextPage<{ user?: User }> = (props: { user?: User }) => {
  const { user } = props;
  return (
    <>
      <LayoutApp user={user}>
        <Head>
          <title>Game of blocks - Dashboard</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Lobbies />
      </LayoutApp>
    </>
  );
};

export default withAuthSync(withApollo(DashboardPage));
