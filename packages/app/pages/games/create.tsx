import React from 'react';

import withApollo from '../../lib/apollo/withApollo';
import withAuthSync from '../../lib/withAuthSync';
import { CreateGame } from '../../components/CreateGame';
import { LayoutApp } from '../../components/Layout';
import { UserProps, AuthPage } from '../../types/pages';

export const CreateGamePage: AuthPage = (props: UserProps) => {
  const { user } = props;
  return (
    <LayoutApp user={user}>
      <CreateGame userId={user.id} />
    </LayoutApp>
  );
};

export default withAuthSync(withApollo(CreateGamePage));
