import React from 'react';
import { useRouter } from 'next/router';
import { Text, Box } from 'theme-ui';
import dynamic from 'next/dynamic';
import { GameTest } from '../../components/GameTest';

import withAuthSync from '../../lib/withAuthSync';
import withApollo from '../../lib/apollo/withApollo';
import { UserProps, AuthPage } from '../../types/pages';

const Map = dynamic(
  () => import('../../components/Map').then((mod) => mod.Map),
  {
    ssr: false,
  }
);

const GamePage: AuthPage = (props: UserProps) => {
  const { user } = props;
  const { query } = useRouter();
  const { id } = query;
  return (
    <Box>
      <Box
        sx={{
          position: 'fixed',
          zIndex: '10',
          width: '400px',
          height: 'auto',
          bg: 'black',
        }}
      >
        <Text sx={{ p: 2 }}>{`Game ${query.id}`}</Text>
        <GameTest userId={user.id} id={id} />
      </Box>
      <Map />
    </Box>
  );
};

export default withAuthSync(withApollo(GamePage));
